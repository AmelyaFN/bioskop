package com.binar.Bioskop.Service.Users;

import com.binar.Bioskop.Repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsersServiceImpl implements UsersService {
    @Autowired
    private UsersRepository userRepository;

    @Override
    public Integer countUsers() {
        return userRepository.countUsers();
    }

    @Override
    public void addUser(String username, String email, String password) {
        userRepository.addUser(username, email, password);
    }

    @Override
    public void updateUser(String username, String email, String password, Integer userId) {
        userRepository.updateUser(username, email, password, userId);
    }

    @Override
    public void deleteUser(Integer userId) {
        userRepository.deleteUser(userId);
    }
}
