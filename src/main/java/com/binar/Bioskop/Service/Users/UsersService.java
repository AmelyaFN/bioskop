package com.binar.Bioskop.Service.Users;

import org.springframework.stereotype.Service;

@Service
public interface UsersService {

    Integer countUsers();

    void addUser(String username, String email, String password);

    void updateUser(String username, String email, String password, Integer userId);

    void deleteUser(Integer userId);

}
