package com.binar.Bioskop.Model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity(name = "seats")
@Getter
@Setter
public class SeatsModel {
    @EmbeddedId
    private SeatId seatId;
}
