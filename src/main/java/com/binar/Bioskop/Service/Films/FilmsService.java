package com.binar.Bioskop.Service.Films;

import com.binar.Bioskop.Model.FilmsModel;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public interface FilmsService {

    Integer countFilms();

    void addFilm(String filmCode, String filmName, Integer sedangTayang);

    void updateFilm(String filmCode, String filmName, Integer sedangTayang, Integer filmId);

    void deleteFilm(Integer filmId);

    void deleteScheduleFilm(Integer filmId);

    List<FilmsModel> showAllFilms();

    // List<ScheduleEntity> showScheduleFilmJpa();

    List<Object[]> showScheduleFilm(Integer filmId);

}
