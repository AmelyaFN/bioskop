package com.binar.Bioskop.Model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity(name = "films")
@Getter
@Setter
public class FilmsModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "film_id")
    private Integer filmId;

    @Column(name = "film_code")
    private String filmCode;

    @Column(name = "film_name")
    private String filmName;

    @Column(name = "sedang_tayang")
    private Integer sedangTayang;

}



