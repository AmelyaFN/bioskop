package com.binar.Bioskop.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface UsersRepository extends JpaRepository {

    // Hitung jumlah data yang ada di DB, return Integer
    @Query("select count(1) from users")
    Integer countUsers();

    // Add a user
    @Modifying
    @Query(nativeQuery = true, value = "insert into users(username, email, password) values(:username, :email, :password)")
    void addUser(
            @Param("username") String username,
            @Param("email") String email,
            @Param("password") String password
    );

    // Update data a user
    @Modifying
    @Query(nativeQuery = true, value = "update users u set u.username= :username, u.email= :email, u.password= :password where u.user_id= :user_id")
    void updateUser(
            @Param("username") String username,
            @Param("email") String email,
            @Param("password") String password,
            @Param("user_id") Integer userId
    );

    // Delete a user
    @Modifying
    @Query(nativeQuery = true, value = "delete from users where user_id= :user_id")
    void deleteUser(
            @Param("user_id") Integer userId
    );

}
