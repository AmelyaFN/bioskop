package com.binar.Bioskop.Controller;

import com.binar.Bioskop.Service.Users.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class UserController {

    @Autowired
    private UsersService userService;

    public void countUsers() {
        System.out.println(userService.countUsers());
    }

    public void addUser(String username, String email, String password) {
        userService.addUser(username, email, password);
    }

    public void updateUser(String username, String email, String password, Integer userId) {
        userService.updateUser(username, email, password, userId);
    }

    public void deleteUser(Integer userId) {
        userService.deleteUser(userId);
    }

}
