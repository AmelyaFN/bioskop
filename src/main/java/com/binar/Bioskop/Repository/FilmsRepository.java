package com.binar.Bioskop.Repository;

import com.binar.Bioskop.Model.FilmsModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface FilmsRepository extends JpaRepository<FilmsModel, Integer> {

    // Hitung jumlah data yang ada di DB, return Integer
    @Query(nativeQuery = true, value = "SELECT COUNT(1) FROM films")
    Integer countFilms();

    // Add Film
    @Modifying
    @Query(nativeQuery = true, value = "insert into films(film_code, film_name, sedang_tayang) values(:film_code, :film_name, :sedang_tayang)")
    void addFilm(
            @Param("film_code") String filmCode,
            @Param("film_name") String filmName,
            @Param("sedang_tayang") Integer sedangTayang
    );

    // Update Film data
    @Modifying
    @Query(nativeQuery = true, value = "update films f set f.film_code= :film_code, f.film_name= :film_name, f.sedang_tayang= :sedang_tayang where f.film_id= :film_id")
    void updateFilm(
            @Param("film_code") String filmCode,
            @Param("film_name") String filmName,
            @Param("sedang_tayang") Integer sedangTayang,
            @Param("film_id") Integer filmId
    );

    // Delete a film
    @Modifying
    @Query(nativeQuery = true, value = "delete from films where film_id= :film_id")
    void deleteFilm(
            @Param("film_id") Integer filmId
    );

    // Delete film schedule
    @Modifying
    @Query(nativeQuery = true, value = "delete from schedules where film_id= :film_id")
    void deleteScheduleFilm(
            @Param("film_id") Integer filmId
    );


    // 2. Menampilkan schedule dari film tertentu
    @Query(nativeQuery = true, value = "select s.* from films f join schedules s on f.film_id = s.film_id where f.film_id = :film_id")
    List<Object[]> showFilmSchedule(
            @Param("film_id") Integer filmId
    );

}
