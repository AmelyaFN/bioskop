package com.binar.Bioskop.Model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity(name = "schedules")
@Getter
@Setter
public class SchedulesModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "schedule_id")
    private Integer scheduleId;

    @ManyToOne(targetEntity = FilmsModel.class) //cascade = CascadeType.DETACH)
    @JoinColumn(name = "film_id")
    private Integer filmId;

    @Column(name = "tanggal_tayang")
    private String tanggalTayang;

    @Column(name = "jam_mulai")
    private String jamMulai;

    @Column(name = "jam_selesai")
    private String jamSelesai;

    @Column(name = "harga_tiket")
    private Integer hargaTiket;


    public SchedulesModel(Integer scheduleId, Integer filmId, String tanggalTayang, String jamMulai, String jamSelesai, Integer hargaTiket) {
        this.scheduleId = scheduleId;
        this.filmId = filmId;
        this.tanggalTayang = tanggalTayang;
        this.jamMulai = jamMulai;
        this.jamSelesai = jamSelesai;
        this.hargaTiket = hargaTiket;
    }
}
