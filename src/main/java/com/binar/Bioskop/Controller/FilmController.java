package com.binar.Bioskop.Controller;

import com.binar.Bioskop.Model.FilmsModel;
import com.binar.Bioskop.Model.SchedulesModel;
import com.binar.Bioskop.Service.Films.FilmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;

@Controller
public class FilmController {

    @Autowired
    private FilmsService filmService;

    public void countFilms() {
        System.out.println(filmService.countFilms());
    }

    public void addFilm(String filmCode, String filmName, Integer sedangTayang) {
        filmService.addFilm(filmCode, filmName, sedangTayang);
    }

    void updateFilm(String filmCode, String filmName, Integer sedangTayang, Integer filmId) {
        filmService.updateFilm(filmCode, filmName, sedangTayang, filmId);
    }

    public void deleteFilm(Integer filmId) {
        filmService.deleteScheduleFilm(filmId);
        filmService.deleteFilm(filmId);
    }

    public void showAllSedangTayang() {
        List<FilmsModel> allFilms = filmService.showAllFilms();

        allFilms.forEach(filmEntity -> {
            if (filmEntity.getSedangTayang() == 1) {
                System.out.println(filmEntity.getFilmName());
            }
        });
    }

    /*public void showScheduleFilm() {
        List<ScheduleEntity> listSchedule = filmService.showScheduleFilmJpa();
        listSchedule.forEach(val -> System.out.println(val.getFilmId()));
    }*/

    public void showScheduleFilm(Integer filmId) {
        List<Object[]> obj = filmService.showScheduleFilm(filmId);
        List<SchedulesModel> lSE = new ArrayList<>();
        for (Object[] objects : obj) {
            SchedulesModel se = new SchedulesModel((Integer) objects[0], (Integer) objects[1], (String) objects[2], (String) objects[3], (String) objects[4], (Integer) objects[5]);
            lSE.add(se);
        }

        lSE.forEach(entity -> {
            System.out.println("Tanggal tayang      : " + entity.getTanggalTayang());
            System.out.println("Jam mulai           : " + entity.getJamMulai());
            System.out.println("Jam selesai         : " + entity.getJamSelesai());
            System.out.println("Harga tiket         : Rp. " + entity.getHargaTiket());
        });
    }

}
