package com.binar.Bioskop.Service.Films;

import com.binar.Bioskop.Model.FilmsModel;
import com.binar.Bioskop.Repository.FilmsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilmsServiceImpl implements FilmsService {

    @Autowired
    private FilmsRepository filmRepository;

    @Override
    public Integer countFilms() {
        return filmRepository.countFilms();
    }

    @Override
    public void addFilm(String filmCode, String filmName, Integer sedangTayang) {
        filmRepository.addFilm(filmCode, filmName, sedangTayang);

    }

    @Override
    public void updateFilm(String filmCode, String filmName, Integer sedangTayang, Integer filmId) {
        filmRepository.updateFilm(filmCode, filmName, sedangTayang, filmId);
    }

    @Override
    public void deleteFilm(Integer filmId) {
        filmRepository.deleteFilm(filmId);
    }

    @Override
    public void deleteScheduleFilm(Integer filmId) {
        filmRepository.deleteScheduleFilm(filmId);
    }

    @Override
    public List<FilmsModel> showAllFilms() {
        return filmRepository.findAll();
    }

    /*@Override
    public List<ScheduleEntity> showScheduleFilmJpa() {
        return IScheduleRepository.findAll();
    }*/


    @Override
    public List<Object[]> showScheduleFilm(Integer filmId) {
        return filmRepository.showFilmSchedule(filmId);
    }

}
